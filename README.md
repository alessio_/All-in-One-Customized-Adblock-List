﻿# All-in-One-Customized-Adblock-List 2.6

![logo](https://i.imgur.com/etKp2Tx.png "Logo") ![sub](https://images2.imgbox.com/61/d6/YkzqktzV_o.png "Sub")

Website: https://hl2guide.github.io/All-in-One-Customized-Adblock-List/
Wiki/FAQs: https://github.com/hl2guide/All-in-One-Customized-Adblock-List/wiki
FilterLists Entry [search for "All-in-One Customized Adblock List", easy subscribe button]: https://filterlists.com

(This repository is actively maintained and updates every 2 hours)

## Description
An all-in-one adblock list that thoroughly blocks trackers, popup ads, ads, unwanted cookies, fake news, cookie warning messages, unwanted comment sections, crypto-coin mining, YouTube clutter, Twitter guff and social network hassles.

Tested in uBlock Origin for Google Chrome, Firefox and Edge; it should work just
fine in any decent adblocker that supports Adblock Plus 2.0 formatted lists.

It includes customized entries that block particularly annoying page elements.
It is actively maintained (updates every 2 hours) and is saved in UTF-8 format. Have fun :D

**(VITAL:)**
Be sure to disable any lists that appear in the list below that you already have active in
the settings/dashboard. e.g. https://i.imgur.com/cVyxL7J.png

## Usage
In your adblocker's filter lists section add either the URL:

https://bit.ly/AIO-adblock

**OR**

https://raw.githubusercontent.com/hl2guide/All-in-One-Customized-Adblock-List/master/deanoman-adblocklist.txt

Check the wiki for step-by-step instructions. (https://github.com/hl2guide/All-in-One-Customized-Adblock-List/wiki)

## News
Version 2.6 of the AIO list is more complete and potent than ever. Optimized, consolidated and duplicate lines are removed :D

## What's in Version 2.6?

* Added more adblock lists
* Tested in browsers: Firefox and Google Chrome
* Tested in addons/extensions: uBlock Origin and Adblock Plus

## Windows Recommendations

1. Personally I use a custom Windows HOSTS file to handle the initial blocking. [updated every 2 days]

    __Steven Black's List (includes social media blocking):__
https://raw.githubusercontent.com/StevenBlack/hosts/master/alternates/fakenews-gambling-social/hosts

2. Then I use my add my AIO list in uBlock Origin within all browsers

3. Then I clear browser caches often and make sure all cookies are removed

## Credits
Full credit goes to original list creators and maintainers, thanks so much for their tireless work! :D

## Filter Lists
90 Lists Included:
* 0131 Block List (Austin Huang) [Removes tedious AD and AD frames from specific popular websites such as New York Times] - https://raw.githubusercontent.com/austinhuang0131/0131-block-list/master/list.txt
* Adblock Warning Removal List (EasyList) [Blocks Adblock warnings on websites.] - https://easylist-downloads.adblockplus.org/antiadblockfilters.txt
* Adblock YouTube Ads (kbinani) [Blocks ads on YouTube.] - https://raw.githubusercontent.com/kbinani/adblock-youtube-ads/master/signed.txt
* AdGuard Annoyances Filter (AdGuard) [Blocks irritating page elements.] - https://filters.adtidy.org/extension/chromium/filters/14.txt
* AdGuard Social Media Filter (AdGuard) [Blocks «Like» and «Tweet» buttons on all the popular websites on the Internet] - https://filters.adtidy.org/extension/chromium/filters/4.txt
* AdGuard Spyware Filter (AdGuard) [Blocks spyware trackers.] - https://filters.adtidy.org/extension/chromium/filters/3.txt
* Adware Filters (EasyList) [Blocks adware.] - https://easylist-downloads.adblockplus.org/adwarefilters.txt
* Android Scum Class (DandelionSprout) [Blocks fake notification counters.] - https://raw.githubusercontent.com/DandelionSprout/adfilt/master/Android%20Scum%20Class%20%E2%80%94%20Fake%20notification%20counters.txt
* Andromeda Filter List (hit3shjain) [Disables some ad and tracking hosts.] - https://raw.githubusercontent.com/hit3shjain/Andromeda-ublock-list/master/hosts.txt
* Anti-'Abuse porn' list for Pixiv (DandelionSprout) [Blocks abuse porn on Pixiv.] - https://raw.githubusercontent.com/DandelionSprout/adfilt/master/AntiAbusePorn.txt
* Anti-Amazon List for Twitch (DandelionSprout) [Blocks some Amazon promotion elements on Twitch.] - https://raw.githubusercontent.com/DandelionSprout/adfilt/master/AntiAmazonListForTwitch.txt
* Anti-Astrology List (DandelionSprout) [Blocks many astrology related links.] - https://raw.githubusercontent.com/DandelionSprout/adfilt/master/AntiAstrologyList.txt
* Anti-Facebook List (Fanboy) [Blocks Facebook trackers.] - https://fanboy.co.nz/fanboy-antifacebook.txt
* Anti-Malware List (DandelionSprout) [Blocks malware.] - https://raw.githubusercontent.com/DandelionSprout/adfilt/master/Dandelion%20Sprout's%20Anti-Malware%20List.txt
* Anti-Notification pre-prompt banners List (DandelionSprout) [Removes some naggy notification prompts.] - https://raw.githubusercontent.com/DandelionSprout/adfilt/master/Anti-'Notification%20pre-prompt%20banners'%20List.txt
* Anti-PopAds (Yhonay) [Blocks shady, annoying pop-under ads from the infamous PopAds ad network.] - https://raw.githubusercontent.com/Yhonay/antipopads/master/popads.txt
* Anti-'Vomit-inducing things' List (DandelionSprout) [Blocks some gross thing on the internet.] - https://raw.githubusercontent.com/DandelionSprout/adfilt/master/AntiVomitList.txt
* Block the EU Cookie Shit List (Mike Cardwell) [Blocks cookie notifications.] - https://raw.githubusercontent.com/r4vi/block-the-eu-cookie-shit-list/master/filterlist.txt
* BlockUnderRadarJunk (UnluckyLuke) [Blocks more junk.] - https://raw.githubusercontent.com/UnluckyLuke/BlockUnderRadarJunk/master/blockunderradarjunk-list.txt
* Browse websites without logging in (DandelionSprout) [Blocks naggy login prompts.] - https://raw.githubusercontent.com/DandelionSprout/adfilt/master/BrowseWebsitesWithoutLoggingIn.txt
* Clickbait Blocklist (cpeterso) [Blocks the most annoying clickbait ads.] - https://raw.githubusercontent.com/cpeterso/clickbait-blocklist/master/clickbait-blocklist.txt
* Clickbait List (endolith) [Blocks clickbait ads.] - https://raw.githubusercontent.com/endolith/clickbait/master/clickbait.txt
* CoinBlocker Domains List Browser (ZeroDot1) [A list to prevent browser mining.] - https://gitlab.com/ZeroDot1/CoinBlockerLists/raw/master/list_browser.txt
* CoinBlocker Domains List Optional (ZeroDot1) [Another list to prevent browser mining.] - https://gitlab.com/ZeroDot1/CoinBlockerLists/raw/master/list_optional.txt
* Cookie Blocklist (Electronic Frontier Foundation) [Blocks cookies.] - https://www.eff.org/files/cookieblocklist.txt
* CrapBlock Annoyances (theel0ja) [Blocks more crap.] - https://crapblock.theel0ja.info/crapblock-annoyances.txt
* deviantJUNK Filter (Frellwits) [Blocks the thumbnails of users that upload junk or stuff in the wrong categories.] - https://raw.githubusercontent.com/lassekongo83/Frellwits-filter-lists/master/deviantJUNKfilter.txt
* Even cleaner news sites (theel0ja) [Removes bloaty elements from news sites.] - https://crapblock.theel0ja.info/even-cleaner-news-sites.txt
* Fake-News List (Fanboy) [Blocks hoax, propanganda and clickbait (fake sites).] - https://raw.githubusercontent.com/ryanbr/fanboy-adblock/master/fake-news.txt
* Fanboy's Annoyance List (Fanboy) [Blocks Social Media content, in-page pop-ups and other annoyances.] - https://easylist.to/easylist/fanboy-annoyance.txt
* Fanboy's Problematic-sites (Fanboy) [Fixes problematic websites.] - https://fanboy.co.nz/fanboy-problematic-sites.txt
* Fanboy's Social Blocking List (Fanboy) [Blocks social media components.] - https://easylist-downloads.adblockplus.org/fanboy-social.txt
* Hello, Goodbye! (bcye) [Blocks chat or helpdesk pop ups.] - https://raw.githubusercontent.com/bcye/Hello-Goodbye/master/filterlist.txt
* Hexxium Creations Threat List (Hexxium Creations) [Blocks threat websites.] - https://raw.githubusercontent.com/HexxiumCreations/threat-list/gh-pages/hexxiumthreatlist.txt
* I Don't Care about Cookies (kiboke) [Disables cookie warning messages.] - https://www.kiboke-studio.hr/i-dont-care-about-cookies/abp/
* I Don't Want to Download Your Browser (DandelionSprout) [Hides browser download banners.] - https://raw.githubusercontent.com/DandelionSprout/adfilt/master/I%20Don't%20Want%20to%20Download%20Your%20Browser.txt
* I Don't Want Your App (Frellwits) [Specifically targets websites that nags people to install their app.] - https://raw.githubusercontent.com/lassekongo83/Frellwits-filter-lists/master/i-dont-want-your-app.txt
* I Hate Overpromoted Games (DandelionSprout) [Hides overpromoted game's banners.] - https://raw.githubusercontent.com/DandelionSprout/adfilt/master/IHateOverpromotedGames.txt
* I'm OK with cookies (Rudloff) [Disables more cookie warning messages.] - https://raw.githubusercontent.com/Rudloff/adblock-imokwithcookies/master/filters.txt
* Ilyakatz's Additional Filters (Ilyakatz) [Removes popup sections on sites that distracts from the main content.] - https://raw.githubusercontent.com/ilyakatz/adblock_filters/master/inpage_popups.txt
* International List (betterwebleon) [Blocks more subscriptions and social pop-ups.] - https://raw.githubusercontent.com/betterwebleon/international-list/master/filters.txt
* JAB Creations Adblock List (John Bilicki III) [Blocks malware.] - https://www.jabcreations.com/downloads/adblock-filters.php
* Lead Generator Blocklist (Rpsl) [Blocks 'how i can help you?' widgets.] - https://raw.githubusercontent.com/Rpsl/adblock-leadgenerator-list/master/list/list.txt
* Linked Insanity Annoyance Rules (Taylor) [Removes filler, upsells, click bait and other low or negative-value annoyances.] - https://raw.githubusercontent.com/taylr/linkedinsanity/master/linkedinsanity.txt
* List for the Freedom of Technological Diversity (DandelionSprout) [Stops naggy website elements.] - https://raw.githubusercontent.com/DandelionSprout/adfilt/master/TechnologicalDiversity.txt
* Minimalist Cryptocurrency Filter List (MrBukLau) [Removes unnecessary elements on certain cryptocurrency websites.] - https://raw.githubusercontent.com/MrBukLau/filter-lists-for-ublock-origin/master/filter%20lists/minimalistcryptocurrencyfilterlist.txt
* Music Filter List (MrBukLau) [Removes unnecessary elements on certain music websites.] - https://raw.githubusercontent.com/MrBukLau/filter-lists-for-ublock-origin/master/filter%20lists/musicfilterlist.txt
* Mute (Brad Conte) [Hides user comments. Surf without the noise.] - http://mute.bradconte.com/mute.txt
* Nano Contrib Filter - Placeholder Buster (jspenguin2017) [Removes empty ads placeholders.] - https://raw.githubusercontent.com/NanoAdblockerLab/NanoContrib/master/dist/placeholder-buster.txt
* Nano Filters (jspenguin2017) [Blocks ads.] - https://raw.githubusercontent.com/NanoAdblocker/NanoFilters/master/NanoFilters/NanoBase.txt
* NoCoin (hoshsadiq) [Prevents browser based coin mining from clogging up your resources.] - https://raw.githubusercontent.com/hoshsadiq/adblock-nocoin-list/master/nocoin.txt
* Nopelist (genediazjr) [Blocks ads, phishing, malicious, analytics, or clickbait websites.] - https://raw.githubusercontent.com/genediazjr/nopelist/master/nopelist.txt
* NothingBlock Filter (Sayomelu) [Blocks ads.] - https://raw.githubusercontent.com/sayomelu/nothingblock/master/filter.txt
* NoTrack Malware Blocklist (QuidsUp) [Blocks malware.] - https://gitlab.com/quidsup/notrack-blocklists/raw/master/notrack-malware.txt
* NT AdBlock List (Nicktabick) [Blocks ads.] - https://bitbucket.org/nicktabick/adblock-rules/raw/master/nt-adblock.txt
* Overlay Blocker (LordBadmintonofYorkshire) [Blocks annoying elements on websites.] - https://raw.githubusercontent.com/LordBadmintonofYorkshire/Overlay-Blocker/master/blocklist.txt
* Personal Blocklist (WaLLy3K) [Blocks ads, trackers etc.] - https://v.firebog.net/hosts/static/w3kbl.txt
* Piperun's iplogger Filter (Piperun) [Blocks websites's who's only purpose is to log your IP.] - https://raw.githubusercontent.com/piperun/iploggerfilter/master/filterlist
* PornList (WowDude) [Blocks ads/popups/fake thumbs/spam bookmark buttons and more from porn websites.] - https://raw.githubusercontent.com/WowDude/PornList/master/PornList.txt
* Press the Attack (Bogachenko) [Blocks ads.] - https://raw.githubusercontent.com/bogachenko/presstheattack/master/presstheattack.txt
* Privacy Filters (metaphoricgiraffe) [Blocks online trackers.] - https://raw.githubusercontent.com/metaphoricgiraffe/tracking-filters/master/trackingfilters.txt
* Privacy List (EasyList) [Improves privacy.] - https://easylist-downloads.adblockplus.org/easyprivacy.txt
* Reddit Trash Removal Service (DandelionSprout) [Cleans up reddit interface.] - https://raw.githubusercontent.com/DandelionSprout/adfilt/master/RedditTrashRemovalService.txt
* Resources Filter List (MrBukLau) [Removes unnecessary elements on certain resource websites.] - https://raw.githubusercontent.com/MrBukLau/filter-lists-for-ublock-origin/master/filter%20lists/resourcesfilterlist.txt
* Rickroll Blacklist (Jamie Wilkinson) [Blocks "rickroll" pranks.] - http://rickrolldb.com/ricklist.txt
* Satterly's Adblock Plus Filters (Satterly) [Blocks ads.] - https://mrsatterly.com/abp_filters.txt
* Skeletal Blocker (SkeletalDemise) [Blocks ads, trackers, junk, etc.] - https://raw.githubusercontent.com/SkeletalDemise/Skeletal-Blocker/master/Skeletal%20Blocker%20List
* Social Filter List (MrBukLau) [Removes unnecessary elements on certain social websites.] - https://raw.githubusercontent.com/MrBukLau/filter-lists-for-ublock-origin/master/filter%20lists/socialfilterlist.txt
* Spam404 (Cameron Dawe) [Protects you from online scams] - https://raw.githubusercontent.com/Dawsey21/Lists/master/adblock-list.txt
* Spies Dislike Us Having Privacy (Taylor) [Blocks various trackers.] - https://raw.githubusercontent.com/taylr/linkedinsanity/master/spies-dislike-us.txt
* Sports Filter List (MrBukLau) [Removes unnecessary elements on certain sport websites.] - https://raw.githubusercontent.com/MrBukLau/filter-lists-for-ublock-origin/master/filter%20lists/sportsfilterlist.txt
* Staying On The Phone Browser (DandelionSprout) [Removes banners that nag to install apps.] - https://raw.githubusercontent.com/DandelionSprout/adfilt/master/stayingonbrowser/Staying On The Phone Browser
* SteamScamSite (PoorPocketsMcNewHold) [Blocks steam scam sites.] - https://raw.githubusercontent.com/PoorPocketsMcNewHold/steamscamsite/master/steamscamsite.txt
* Strappazzon's Annoyances filter list (Strappazzon) [Blocks cookie notices, modals and other useless elements.] - https://raw.githubusercontent.com/Strappazzon/filterlists/master/Filterlists/Annoyances.txt
* Streaming Filter List (MrBukLau) [Removes unnecessary elements on certain streaming websites.] - https://raw.githubusercontent.com/MrBukLau/filter-lists-for-ublock-origin/master/filter%20lists/streamingfilterlist.txt
* Thoughtconverge's Custom AdBlock Filters (thoughtconverge) [Blocks advertisements and trackers.] - https://raw.githubusercontent.com/thoughtconverge/abf/master/abf.txt
* Twitch - Pure Viewing Experience (DandelionSprout) [Removes distracting elements on Twitch.] - https://raw.githubusercontent.com/DandelionSprout/adfilt/master/uBO%20list%20extensions/TwitchPureViewingExperience-uBOExtension.txt
* uBlock Filters Plus (IDKwhattoputhere) [Additional filters for uBlock Origin.] - https://raw.githubusercontent.com/IDKwhattoputhere/uBlock-Filters-Plus/master/uBlock-Filters-Plus.txt
* uBlock Personal Filters (uBlock-user) [More filters for uBlock Origin.] - https://raw.githubusercontent.com/uBlock-user/uBO-Personal-Filters/master/uPF.txt
* uBOPa (nimasaj) [Blocks ads.] - https://raw.githubusercontent.com/nimasaj/uBOPa/master/uBOPa.txt
* Web Annoyances Ultralist (yourduskquibbles) [Removes web clutter.] - https://raw.githubusercontent.com/yourduskquibbles/webannoyances/master/ultralist.txt
* Website Stretcher (DandelionSprout) [Adds support for wider monitors.] - https://raw.githubusercontent.com/DandelionSprout/adfilt/master/Dandelion Sprout's Website Stretcher.txt
* Whale Filter (eEIi0A5L) [Blocks coin mining.] - https://raw.githubusercontent.com/eEIi0A5L/adblock_filter/master/kujira_filter.txt
* Wikia - Pure Browsing Experience (DandelionSprout) [Better browsing for wikia websites.] - https://raw.githubusercontent.com/DandelionSprout/adfilt/master/WikiaPureBrowsingExperience.txt
* XXX Ad Filter List (eEIi0A5L) [Blocks ads on popular XXX websites.] - https://raw.githubusercontent.com/eEIi0A5L/adblock_filter/master/ichigo_filter.txt
* You's List (Chayoung You) [Blocks even more ads.] - https://raw.githubusercontent.com/yous/YousList/master/youslist.txt
* YouTube - Even More Pure Video Experience (DandelionSprout) [Removes even more (number-wise) distracting things on YouTube.] - https://raw.githubusercontent.com/DandelionSprout/adfilt/master/YouTubeEvenMorePureVideoExperience.txt
* YouTube Pure Video Experience (EasyList) [Blocks all YouTube annoyances.] - https://easylist-downloads.adblockplus.org/yt_annoyances_full.txt
* YouTube Remove Comments (EasyList) [Blocks YouTube comments.] - https://easylist-downloads.adblockplus.org/yt_annoyances_comments.txt
* YouTube Remove Youtube Suggestions (EasyList) [Blocks YouTube suggestions.] - https://easylist-downloads.adblockplus.org/yt_annoyances_suggestions.txt